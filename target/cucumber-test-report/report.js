$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/FitchConnectValidLogin.feature");
formatter.feature({
  "name": "Login function test of www.fitchconnect.com",
  "description": "\tDescription: Test to check whether a user can follow the process to login and logout of Member home page.",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User enters valid username and password to enter authorized user Member home page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@FunctionalTest"
    },
    {
      "name": "@SmokeTest"
    },
    {
      "name": "@SanityTest"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User opens test browser",
  "keyword": "Given "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_opens_test_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User goes to \"https://www.fitchconnect.com\" website",
  "keyword": "When "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_goes_to_website(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks LOGIN",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_clicks_LOGIN()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks Fitch Connect",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_clicks_Fitch_Connect()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is taken to Fitch Connect login page",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_is_taken_to_Fitch_Connect_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters \"Mia\" as username",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_enters_as_username(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters \"Connect1\" as password",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_enters_as_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Log In button",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_clicks_on_Log_In_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is taken to member home page",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_is_taken_to_member_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks log out",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_clicks_log_out()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User closes test browser",
  "keyword": "Then "
});
formatter.match({
  "location": "FitchConnectValidLogin.user_closes_test_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});