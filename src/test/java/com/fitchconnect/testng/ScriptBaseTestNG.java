package com.fitchconnect.testng;

import org.eclipse.jetty.client.WWWAuthenticationProtocolHandler;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.fitchconnect.framework.DriverFactory;

public class ScriptBaseTestNG {

	protected WebDriver driver=null;
	
	@BeforeTest
	@Parameters("browser")
	
	public void setUp(@Optional("chrome") String browser) {
	driver=DriverFactory.initialize(browser).getdriver();
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}
}
